
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <title>Calculadora!</title>
    </head>

    <body>

    </head>
<body>

<div class="container mt-2">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h3>Calculadora de interes simple!</h3>
                </div>
                <div class="card-body">
    <form  name="form" action="calculoController" method="POST">
       <div class="form-group">
           <label for="capital">Capital</label>
           <input class="form-control" type="number" name="capital" required>
        </div>
        <div class="form-group">
            <label for="interes">Tasa interés anual</label>
            <input class="form-control" type="number" name="interes" required>
        </div>
        <div class="form-group">
            <label for="numero">Número de años </label>
            <input class="form-control" type="number" name="numero" required>
        </div>
        <button type="submit" class="mt-2 btn btn-outline-success btn-block">Calcular</button>

    </form>
    
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <h3>Nombre: Jose Era</h3>
                  <h4>Evaluacio unidad 1, APLICACIONES EMPRESARIALES.</h4>
              </div>
          </div>
      </div>

</body>
</html>
